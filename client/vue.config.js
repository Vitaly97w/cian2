module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    compress: true,
    public: "mx.vncc.ru",
    proxy: "http://mx.vncc.ru:5000/",
  },
}
