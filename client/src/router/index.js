import Vue from 'vue'
import VueRouter from 'vue-router'
import Ads from '../views/Ads.vue'
import Selection from '../views/Selection.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Ads',
    component: Ads
  },
  {
    path: '/selection/:id',
    name: 'Selection',
    component: Selection
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
