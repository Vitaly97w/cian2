const Ads = require('../models/ads')

module.exports.getAll = async function (req, res) {
  try {
    // const news = await News.find({}, { text: 1, date: 1 }).sort({ date: -1 })
    const ads = await Ads.find()
    res.status(200).send(ads)
  } catch (error) {
    console.log(error)
  }
}
module.exports.get = async function (req, res) {
  try {
    const ad = await Ads.find({ _id: req.params.id })
    res.status(200).send(ad)
  } catch (error) {
    console.log(error)
  }

}
