const Selection = require('../models/selection')
const Schema = require('mongoose')
module.exports.getAll = async function (req, res) {
  try {
    const selection = await Selection.find()
    res.status(200).send(selection)
  } catch (error) {
    console.log(error)
  }

}
module.exports.get = async function (req, res) {
  try {
    const selection = await Selection.find({ _id: req.params.id })
    res.status(200).send(selection)
  } catch (error) {
    console.log(error)
  }

}
module.exports.put = async function (req, res) {
  try {
    const selection = await Selection.updateOne({ _id: req.params.id }, {
      $set: {
        ofAds: req.body,
        isOtvetil: true,
      }
    })
    res.status(200).send(selection)
  } catch (error) {
    console.log(error)
  }



}
module.exports.post = async function (req, res) {
  try {
    let arr = []
    for (let a in req.body) {
      arr.push(req.body[a])
    }
    const selection = new Selection(
      {
        ofAds: arr
      }
    )
    await selection.save()
    res.status(200).send(selection._id)
  } catch (error) {
    console.log(error)
  }
 
}
