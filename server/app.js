const express = require('express')
const mongoose = require('mongoose')
const keys = require('./config/keys')
const cors = require('cors')
const bodyParser = require('body-parser')
//роуты
const ads = require('./routes/ads')
const set = require('./routes/selection')
const app = express()

mongoose
  .connect(keys.mongoURL)
  .then(() => console.log('MongoDB connected'))
  .catch((error) => console.log(error))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.json())
app.use(
  cors({
    credentials: true,
    origin: '*',
  })
)

app.use('/ads', ads)
app.use('/selection', set)
app.use(express.static('uploads'))

module.exports = app
