const express = require('express')
const controller = require('../controllers/selection')
const router = express.Router()

router.get('/', controller.getAll)
router.post('/', controller.post)
router.get('/:id', controller.get)
router.put('/:id', controller.put)

module.exports = router
