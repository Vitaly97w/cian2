const { Schema, model } = require('mongoose')

const schema = new Schema({
  price: Number,
  publication_date: Date,
  images: Array,
  location: {
    state: String,
    city_town: String,
    street: String,
    apart_number: Number,
  },
  property_rights: String,
  online_display: Boolean,
  about_apartment: {
    housing_type: String,
    floor: Number,
    floors_in_house: Number,
    number_of_rooms: Number,
    balcony: Boolean,
    loggia: Boolean,
    total_area: Number,
    living_space: Number,
    repair: String,
    trim: String,
    technics: Array,
  },
})

module.exports = model('ads', schema)
